(ns workbook.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;;* Reverse a String
(defn reverse-string [i]
  (apply str (reduce conj '() i)))
;;(reverse-string "This is a test!!")

;;* Clojure has get-in, assoc-in and update-in but no dissoc-in.  Implement dissoc-in.
(defn dissoc-in [m v]
  (assoc-in m (drop-last v)
            (-> (get-in m (drop-last v))
                (dissoc (last v)))))
;;(dissoc-in {:a {:b 0 :c 1}} [:a :b])

;;* Given a map, get all the paths into the map.
(defn paths [m]
  (set
   (mapcat
    (fn [r]
      (tree-seq (fn [x] (map? (get-in m x)))
                (fn [y] (map #(conj y %)
                             (keys (get-in m y)))) r))
    (map vector (keys m)))))
;;(paths {:a {:b {:c true}} :d {:e true} :f true})

;;* We need to redact certain values from a map for compliance reasons ie tokens, passwords, etc before we log the values.  Write a function redact-vals that redacts values for a given set of keys.
(defn redact [m k]
  (reduce #(assoc-in %1 %2 "[REDACTED]")
          m (filter (fn [j] (= (last j) k))
                    (paths m))))

(defn redact-vals [s m]
  (reduce
   #(redact %1 %2) m s))
;;(redact-vals #{:c} {:a "foo" :b {:c "baz" :d 23}})

;;* Write a function map-invert+ which is like `set/map-invert`, but if the value is a collection, a key for each collection value is created mapped to the original key. ie
(defn rev [i]
  (apply merge (map #(hash-map % (first i)) (second i))))

(defn map-invert+ [d]
  (apply merge-with hash-map (map #(rev %) d)))

(defn map-invert+ [d]
  (reduce-kv #(assoc %1 %2 (if (map? %3) (apply set %3) #{%3}))
             {}
             (apply merge-with hash-map (map #(rev %) d))))

;;(map-invert+ {:a [1 2] :b [3 4] :c [4 5]})

(defn get-next [mp v]
  (if (map? (get-in mp v))
    (map #(conj v %) (keys (get-in mp v)))
    [v]))

(defn build-paths [mp i]
  (let [return-value (mapcat #(get-next mp %) i)]
    (if (not= i return-value)
      (build-paths mp return-value)
      return-value)))

(defn leaf-paths [m]
  (->> m (keys) (map vector) (build-paths m)))
#_(leaf-paths {:a {:b {:c true}}
             :d {:e true
                 :g {:h {:i true}}}
             :f true})
